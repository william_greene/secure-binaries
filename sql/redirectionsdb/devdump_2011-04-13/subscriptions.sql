-- MySQL dump 10.11
--
-- Host: localhost    Database: awesm_production
-- ------------------------------------------------------
-- Server version	5.0.75-0ubuntu10.3-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL auto_increment,
  `customer_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `primary_account_id` int(11) default NULL,
  `package_id` int(11) NOT NULL,
  `voucher_id` int(11) default NULL,
  `updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL default '0000-00-00 00:00:00',
  `expires` timestamp NULL default NULL,
  `subscription_key` varchar(255) NOT NULL,
  `status` enum('active','cancelled','cancelling','suspended') default 'active',
  PRIMARY KEY  (`id`),
  KEY `index_on_customer_id` (`customer_id`),
  KEY `index_on_primary_account_id` (`primary_account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `subscriptions`
--

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
INSERT INTO `subscriptions` VALUES (4,3,'testaccount_3',948,7,NULL,'2011-03-29 01:02:28','2010-10-07 01:01:40','2011-10-07 01:01:40','sub-DO7fK7uR','active'),(8,10,'lionside',958,7,NULL,'2011-03-29 01:02:28','2010-10-19 22:52:21','2011-10-19 22:52:21','sub-WcGh3d7F','active'),(10,12,'afmarkfish2010',960,7,NULL,'2011-03-29 01:02:28','2010-10-20 18:14:10','2011-10-20 18:14:10','sub-k5VELMkS','active'),(11,6,'Hennessy',957,4,NULL,'2011-03-29 01:02:28','2010-10-22 08:27:49','2011-10-22 08:27:49','sub-hbRtvxB8','active'),(12,13,'noisylittlemonkey',963,4,NULL,'2011-03-29 01:02:28','2010-10-27 19:22:09','2011-10-27 19:22:09','sub-LJofQi7w','active'),(13,14,'proactive',964,7,NULL,'2011-03-29 01:02:28','2010-10-28 05:48:12','2011-10-28 05:48:12','sub-LOeO2len','active'),(14,15,'testaccount_5',965,7,NULL,'2011-03-29 01:02:28','2010-10-28 17:35:42','2011-10-28 17:35:42','sub-q91CVlfD','active'),(20,20,'halogen_network',972,7,NULL,'2011-03-29 01:02:29','2010-11-06 00:19:04','2011-11-06 00:19:04','sub-N66I3IRO','active'),(21,22,'GMSL',973,4,NULL,'2011-03-29 01:02:29','2010-11-09 17:14:58','2011-11-09 17:14:58','sub-bF41yCyJ','active'),(25,26,'testaccount_6',979,4,NULL,'2011-03-29 01:02:29','2010-11-15 20:23:45','2011-11-15 20:23:45','sub-bchdOMQF','active'),(27,28,'Bagcheck',982,7,NULL,'2011-03-29 01:02:29','2010-11-18 21:29:31','2011-11-18 21:29:31','sub-VSR0D6Eb','active'),(33,34,'Complans',988,4,NULL,'2011-03-29 01:02:29','2010-12-01 10:37:46','2011-12-01 10:37:46','sub-CLuGdMfE','active'),(38,39,'MADE',995,4,NULL,'2011-03-29 01:02:29','2010-12-09 19:32:24','2011-12-09 19:32:24','sub-bjGawFuX','active'),(41,42,'bitflipping',998,4,NULL,'2011-03-29 01:02:29','2010-12-15 10:26:35','2011-12-15 10:26:36','sub-sbAHybWT','active'),(43,44,'testaccount_8',1000,7,NULL,'2011-03-29 01:02:29','2010-12-17 01:25:55','2011-12-17 01:25:55','sub-3xUH4isH','active'),(44,45,'HerdBoy',1001,8,NULL,'2011-03-29 01:02:29','2010-12-22 08:29:17','2011-12-22 08:29:17','sub-3nXh9Cwl','active'),(46,47,'weforum',1004,4,NULL,'2011-03-29 01:02:29','2011-01-03 16:37:03','2012-01-03 16:37:03','sub-wCLsSFQL','active'),(54,53,'chadianzi',1016,4,NULL,'2011-03-29 01:02:29','2011-01-17 07:05:54','2012-01-17 07:05:54','sub-rbcpC8IF','active'),(56,55,'MyExcellus',1018,7,NULL,'2011-03-29 01:02:29','2011-01-18 21:23:22','2012-01-18 21:23:22','sub-6nXAWPRZ','active'),(57,56,'MyUnivera',1019,7,NULL,'2011-03-29 01:02:29','2011-01-18 21:26:08','2012-01-18 21:26:08','sub-DyHMBD78','active'),(58,57,'andrewhyde',1022,8,NULL,'2011-03-29 01:02:29','2011-01-20 23:13:10','2012-01-20 23:13:10','sub-QT1siREa','active'),(60,59,'babypips',1025,4,NULL,'2011-03-29 01:02:29','2011-01-24 13:00:49','2012-01-24 13:00:49','sub-2rPFziU6','active'),(62,61,'aventineco',1027,4,NULL,'2011-03-29 01:02:29','2011-01-26 15:33:08','2012-01-26 15:33:08','sub-V2G2Ricf','active'),(68,67,'x1webdev',1036,4,NULL,'2011-03-29 01:02:29','2011-02-07 22:47:49','2012-02-07 22:47:49','sub-ht1s691W','active'),(69,68,'SalesVote',1037,8,NULL,'2011-03-29 01:02:29','2011-02-08 15:15:41','2012-02-08 15:15:41','sub-c2PLTevW','active'),(70,69,'lewis',1039,8,NULL,'2011-03-29 01:02:29','2011-02-08 20:38:07','2012-02-08 20:38:07','sub-VKlV3GbZ','active'),(73,75,'crowdtap',1041,7,NULL,'2011-03-29 01:02:29','2011-02-10 00:17:22','2012-02-10 00:17:22','sub-IsbkaDPI','active'),(78,80,'Rockfish',1052,8,NULL,'2011-03-29 01:02:29','2011-02-17 16:48:28','2012-02-17 16:48:28','sub-6RADaBkD','active'),(85,82,'testaccount_12',1054,5,NULL,'2011-03-29 01:02:29','2011-02-18 21:50:15','2012-02-18 21:50:16','sub-EAYhoTDJ','active'),(88,85,'jenniferlum',1058,8,NULL,'2011-03-29 01:02:29','2011-02-23 13:36:48','2012-02-23 13:36:49','sub-oyorFQRX','active'),(89,86,'Tippr',1059,8,NULL,'2011-03-29 01:02:29','2011-02-23 19:17:17','2012-02-23 19:17:17','sub-J2HtFgBM','active'),(91,88,'Bizzy',1061,7,NULL,'2011-03-29 01:02:29','2011-02-25 00:56:44','2012-02-25 00:56:44','sub-7MaiYKvm','active'),(92,89,'BBCEarth',1063,7,NULL,'2011-03-29 01:02:29','2011-02-28 12:06:30','2012-02-28 12:06:30','sub-UTUIxicl','active'),(93,90,'bloodhound',1068,4,NULL,'2011-03-29 01:02:29','2011-03-01 22:45:45','2012-02-29 22:45:45','sub-hRdwr4TA','active'),(94,91,'pixelpopdev',1069,8,NULL,'2011-03-29 01:02:29','2011-03-02 00:50:52','2012-03-01 00:50:52','sub-6k4MRGjZ','active'),(96,93,'GPYR',1073,4,NULL,'2011-03-30 07:29:38','2011-03-03 05:37:54','2012-03-02 05:37:54','sub-dJHBeCy8','cancelling'),(97,94,'procurement',1076,4,NULL,'2011-03-29 01:02:29','2011-03-05 13:37:32','2012-03-04 13:37:33','sub-gTRdB3Ps','active'),(98,95,'wavonomics',1077,4,NULL,'2011-04-10 20:25:42','2011-03-07 16:20:04','2012-03-06 16:20:04','sub-V2oM6IXD','cancelling'),(99,96,'thedailylove',1078,4,NULL,'2011-03-29 01:02:29','2011-03-07 21:34:46','2012-03-06 21:34:46','sub-3cZUsJ3W','active'),(100,97,'ColliderMedia',1079,4,NULL,'2011-03-29 01:02:29','2011-03-08 22:57:53','2012-03-07 22:57:53','sub-cLiHYgPE','active'),(101,98,'vpromos',1080,4,NULL,'2011-03-29 01:02:29','2011-03-09 22:52:08','2012-03-08 22:52:08','sub-ArsLuHEP','active'),(102,99,'conversocial',1082,8,NULL,'2011-03-29 01:02:29','2011-03-11 00:49:30','2012-03-10 00:49:31','sub-J3mQLKdO','active'),(103,100,'WalshJesuit',1083,4,NULL,'2011-03-29 01:02:29','2011-03-11 15:46:22','2012-03-10 15:46:22','sub-wDspWwL9','active'),(104,103,'cardinal',1084,4,NULL,'2011-03-29 01:02:29','2011-03-16 22:47:29','2012-03-15 22:47:29','sub-I3Qqkq4v','active'),(105,104,'percolater',1085,7,NULL,'2011-03-29 01:02:29','2011-03-18 05:33:43','2012-03-17 05:33:44','sub-hxGcEV1Y','active'),(106,106,'Zurnal24',1086,4,NULL,'2011-03-29 01:02:29','2011-03-23 13:20:42','2012-03-22 13:20:42','sub-Yoyt9cI5','active'),(107,107,'Porter_Logics',1087,7,NULL,'2011-03-29 01:02:30','2011-03-23 18:18:22','2012-03-22 18:18:23','sub-pAvM7HWP','active'),(108,108,'demandmedia',1089,4,NULL,'2011-03-29 01:02:30','2011-03-23 22:28:16','2012-03-22 22:28:16','sub-LxF6nK1U','active'),(109,109,'ValSMO',1090,4,NULL,'2011-03-29 01:02:30','2011-03-24 14:03:56','2012-03-23 14:03:56','sub-HI6Wn84M','active'),(110,110,'cogencymalcolm',1091,4,NULL,'2011-03-29 01:02:30','2011-03-25 01:40:43','2012-03-24 01:40:43','sub-x3F6gXCW','active'),(111,112,'alleyoop',1094,4,NULL,'2011-03-29 01:02:30','2011-03-28 16:27:36','2012-03-27 16:27:36','sub-i8tqQPGl','active'),(112,113,'AWA',1095,4,NULL,'2011-03-29 01:02:30','2011-03-28 16:28:01','2012-03-27 16:28:01','sub-MVs9que8','active'),(113,117,'affiliatemanager',1096,4,NULL,'2011-03-29 01:02:30','2011-03-28 19:01:54','2012-03-27 19:01:54','sub-Cle0ujNs','active'),(114,118,'storenvy',1097,7,NULL,'2011-03-29 01:02:30','2011-03-28 19:54:12','2012-03-27 19:54:12','sub-ndyTBB0t','active'),(115,119,'wjohn928',1098,8,NULL,'2011-03-29 01:02:30','2011-03-28 23:42:28','2012-03-27 23:42:28','sub-JtkkI0Vf','active'),(116,120,'testsub_1',NULL,0,NULL,'2011-03-29 01:08:20','2011-03-29 01:08:20',NULL,'sub-LmitJpCC','active'),(117,123,'KCRW',NULL,0,NULL,'2011-03-29 21:03:23','2011-03-29 21:03:23',NULL,'sub-2jTTlphG','active'),(118,124,'TheUnderstatement',NULL,0,NULL,'2011-03-30 11:05:48','2011-03-30 11:05:48',NULL,'sub-FOJCOwia','active'),(119,125,'testsub_2',NULL,0,NULL,'2011-03-31 02:57:35','2011-03-31 02:57:35',NULL,'sub-XGQmNrRN','active'),(120,126,'Care2Trailblazers',NULL,0,NULL,'2011-04-01 20:33:56','2011-04-01 20:33:56',NULL,'sub-M6PGewbD','active'),(121,127,'launchrock',NULL,0,1,'2011-04-04 21:10:38','2011-04-04 21:10:38',NULL,'sub-OHoT6woz','active'),(122,128,'amir',NULL,0,NULL,'2011-04-04 21:48:57','2011-04-04 21:48:57',NULL,'sub-8mUTlWPM','active'),(123,129,'mindjolt',NULL,0,NULL,'2011-04-06 02:18:00','2011-04-06 02:18:00',NULL,'sub-LAHjmUjp','active'),(124,130,'ArielGroup040611',NULL,0,NULL,'2011-04-12 17:31:43','2011-04-06 15:56:42',NULL,'sub-LdRpnrKG','cancelling'),(125,131,'wedgewood',NULL,0,12,'2011-04-06 22:47:23','2011-04-06 22:47:23',NULL,'sub-8F3UHfHY','active'),(126,132,'test556',NULL,0,1,'2011-04-07 00:58:18','2011-04-07 00:58:18',NULL,'sub-M05CsP9V','active'),(127,133,'clubsodaandsalt',NULL,0,7,'2011-04-07 02:18:36','2011-04-07 02:18:36',NULL,'sub-aUIwh7Im','active'),(128,134,'project1',NULL,0,NULL,'2011-04-10 15:00:58','2011-04-07 21:42:03',NULL,'sub-g832yAZt','cancelling'),(129,135,'bigworld',NULL,0,NULL,'2011-04-09 19:19:45','2011-04-09 19:19:45',NULL,'sub-vyVTX6Gp','active'),(130,136,'HighOnScore',NULL,0,NULL,'2011-04-11 09:18:58','2011-04-11 09:08:05',NULL,'sub-NqOFHc0W','cancelling'),(131,137,'beatum',NULL,0,NULL,'2011-04-11 18:11:06','2011-04-11 18:11:06',NULL,'sub-iUCneCaW','active'),(132,138,'widgetnc',NULL,0,NULL,'2011-04-11 21:58:12','2011-04-11 21:58:12',NULL,'sub-jhPvPETa','active'),(133,139,'zaarly',NULL,0,NULL,'2011-04-11 23:02:35','2011-04-11 23:02:35',NULL,'sub-GSnHLcIh','active'),(134,140,'ymcadc',NULL,0,10,'2011-04-12 20:18:32','2011-04-12 20:18:32',NULL,'sub-YRFJSbgf','active'),(135,141,'jec-conversion-landing-page-test',NULL,0,12,'2011-04-12 23:11:29','2011-04-12 23:11:29',NULL,'sub-HbSHZgZX','active'),(136,143,'jec-test2',NULL,0,1,'2011-04-13 01:30:37','2011-04-13 01:30:37',NULL,'sub-hUKOj25V','active');
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-04-13 19:49:04
