-- MySQL dump 10.11
--
-- Host: localhost    Database: awesm_production
-- ------------------------------------------------------
-- Server version	5.0.75-0ubuntu10.3-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `applications` (
  `id` int(11) NOT NULL auto_increment,
  `application_key` varchar(255) default NULL,
  `name` varchar(255) default NULL,
  `label` varchar(255) default NULL,
  `description` text,
  `icon` varchar(2000) default NULL,
  `icon_small` varchar(2000) default NULL,
  `url` varchar(2000) default NULL,
  `developer_name` varchar(255) default NULL,
  `developer_url` varchar(2000) default NULL,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `index_applications_on_name` (`name`),
  UNIQUE KEY `index_applications_on_label` (`label`),
  UNIQUE KEY `index_applications_key` (`application_key`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `applications`
--

LOCK TABLES `applications` WRITE;
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;
INSERT INTO `applications` VALUES (1,'voaRWl','AddThis','addthis','The #1 bookmarking & sharing service','http://s3.amazonaws.com/public.awe.sm/img/applications/addthis_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/addthis_icon_small.png','http://tools.awe.sm/addthis/','AddThis','http://addthis.com',NULL,NULL),(2,'D94v3F','Gilt Groupe','gilt','Gilt Groupe provides invitation-only access to highly coveted products and experiences at insider prices','http://s3.amazonaws.com/public.awe.sm/img/applications/gilt_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/gilt_icon_small.png','http://tools.awe.sm/gilt/','Gilt Groupe','http://www.gilt.com/',NULL,NULL),(3,'KtnKGd','Flowtown','flowtown','Social media marketing made simple','http://s3.amazonaws.com/public.awe.sm/img/applications/flowtown_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/flowtown_icon_small.png','http://tools.awe.sm/flowtown/','Flowtown','http://flowtown.com',NULL,NULL),(4,'SLIjFd','FT Tilt','ft_tilt','A premium online financial news and analysis service from the Financial Times','http://s3.amazonaws.com/public.awe.sm/img/applications/ft_tilt_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/ft_tilt_icon_small.png','http://tools.awe.sm/ft_tilt/','FT Tile','http://tilt.ft.com/',NULL,NULL),(5,'ggVf2y','AddToAny','addtoany','The world\'s best sharing button','http://s3.amazonaws.com/public.awe.sm/img/applications/addtoany_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/addtoany_icon_small.png','http://tools.awe.sm/addtoany/','AddToAny','http://addtoany.com',NULL,NULL),(6,'3PUA9N','Vevo','vevo','The leading premium music video and entertainment service','http://s3.amazonaws.com/public.awe.sm/img/applications/vevo_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/vevo_icon_small.png','http://tools.awe.sm/vevo/','Vevo','http://vevo.com',NULL,NULL),(7,'09begV','Storify','storify','Create stories using social media','http://s3.amazonaws.com/public.awe.sm/img/applications/storify_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/storify_icon_small.png','http://tools.awe.sm/storify/','Storify','http://storify.com',NULL,NULL),(8,'kEoHrR','awe.sm','awesm','Sharing tools built by the awe.sm team','http://s3.amazonaws.com/public.awe.sm/img/applications/awesm_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/awesm_icon_small.png','http://tools.awe.sm/awesm/','awe.sm','http://tools.awe.sm',NULL,NULL),(9,'nZurcZ','Test','test','A test application for developers','',NULL,'http://tools.awe.sm/test/','Test Developer','http://developers.awe.sm/',NULL,NULL),(10,'tlVC3D','Topspin','topspin','Direct-to-fan marketing tools for music artists','http://s3.amazonaws.com/public.awe.sm/img/applications/topspin_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/topspin_icon_small.png','http://tools.awe.sm/topspin/','Topspin Media','http://topspinmedia.com',NULL,NULL),(11,'MlJV5B','BigDoor','bigdoor','Use game mechanics to increase user engagement, loyalty and monetization','http://s3.amazonaws.com/public.awe.sm/img/applications/bigdoor_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/bigdoor_icon_small.png','http://tools.awe.sm/bigdoor/','BigDoor Media','http://bigdoor.com',NULL,NULL),(12,'Bmgx0U','Bagcheck','bagcheck','Give your friends a peek into the things you love','http://s3.amazonaws.com/public.awe.sm/img/applications/bagcheck_icon.png','http://s3.amazonaws.com/public.awe.sm/img/applications/bagcheck_icon_small.png','http://tools.awe.sm/bagcheck/','Bagcheck','http://bagcheck.com',NULL,NULL),(13,'app-WOpMIT','timely','timely',NULL,NULL,NULL,NULL,NULL,NULL,'2011-01-19 20:42:42','2011-01-19 20:42:42'),(14,'app-UVF9VA','viplist','viplist',NULL,NULL,NULL,NULL,NULL,NULL,'2011-02-01 23:56:03','2011-02-01 23:56:03'),(15,'app-ts2hV1','tap11','tap11',NULL,NULL,NULL,NULL,NULL,NULL,'2011-02-10 17:40:43','2011-02-10 17:40:43'),(16,'app-BgCBaW','salesvote','salesvote',NULL,NULL,NULL,NULL,NULL,NULL,'2011-02-15 17:54:00','2011-02-15 17:54:00'),(17,'app-4LGNLm','stagebloc','stagebloc',NULL,NULL,NULL,NULL,NULL,NULL,'2011-02-15 17:54:00','2011-02-15 17:54:00'),(18,'app-2rArES','bizzy','bizzy',NULL,NULL,NULL,NULL,NULL,NULL,'2011-03-01 18:19:51','2011-03-01 18:19:51'),(19,'app-LjxatT','percolater','percolater',NULL,NULL,NULL,NULL,NULL,NULL,'2011-03-18 21:49:51','2011-03-18 21:49:51');
/*!40000 ALTER TABLE `applications` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-04-13 19:49:03
