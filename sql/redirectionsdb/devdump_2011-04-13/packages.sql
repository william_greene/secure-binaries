-- MySQL dump 10.11
--
-- Host: localhost    Database: awesm_production
-- ------------------------------------------------------
-- Server version	5.0.75-0ubuntu10.3-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `packages` (
  `id` int(11) NOT NULL auto_increment,
  `label` varchar(200) character set latin1 NOT NULL,
  `display_name` varchar(500) character set latin1 NOT NULL,
  `description` varchar(2000) character set latin1 NOT NULL,
  `price_setup` int(11) default NULL,
  `price_per_period` int(11) default NULL,
  `price_period` enum('week','month','year') character set latin1 NOT NULL,
  `updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL default '0000-00-00 00:00:00',
  `is_available` bit(1) default NULL,
  PRIMARY KEY  (`id`),
  KEY `index_on_is_available` (`is_available`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
INSERT INTO `packages` VALUES (1,'legacy-free','Legacy free package - $0','Free subscription granted by awe.sm staff',0,0,'month','2011-03-29 01:02:30','2010-10-04 01:46:05','\0'),(2,'legacy-annual','Legacy annual package - $99/year','Annual package for customers who joined during the awe.sm beta period',0,9900,'year','2011-03-29 01:02:30','2010-10-04 01:46:05','\0'),(3,'legacy-monthly','Legacy monthly package - $8.25/month','Monthly package for customers who joined during the awe.sm beta period',0,825,'year','2011-03-29 01:02:30','2010-10-04 01:46:05','\0'),(4,'publisher-starter','Publisher Starter Package - $129','5 users, 25,000 links/month, 1,000 API calls/month',0,12900,'month','2010-10-20 01:25:46','2010-10-04 01:46:05',''),(5,'publisher-pro','Publisher Pro Package - $249','10 users, 100,000 links/month, 5,000 API calls/month',0,24900,'month','2010-10-20 01:25:46','2010-10-04 01:46:05',''),(6,'publisher-enterprise','Publisher Enterprise Package - $449','25 users, 250,000 links/month, 10,000 API calls/month',0,44900,'month','2010-10-20 01:25:46','2010-10-04 08:46:05',''),(7,'developer-basic','Developer Basic Package - $69','1 account, 1 user, 10,000 links/month, 1,000 API calls/month',0,6900,'month','2010-10-20 01:25:46','2010-10-04 01:46:05',''),(8,'free-basic','Free account! - $0','Try out our service for free',0,0,'month','2011-03-29 01:02:30','2010-10-15 01:45:00','\0');
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-04-13 19:49:03
