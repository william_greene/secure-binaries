-- MySQL dump 10.11
--
-- Host: localhost    Database: awesm_production
-- ------------------------------------------------------
-- Server version	5.0.75-0ubuntu10.3-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `channels` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `stored_as` varchar(255) default NULL,
  `category` varchar(255) default NULL,
  `destination_append` varchar(2000) default NULL,
  `destination_embed` varchar(2000) default NULL,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `index_channels_on_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `channels`
--

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
INSERT INTO `channels` VALUES (1,'twitter','twitter','twitter','http://twitter.com/home?status=CONTENT+AWESM_TARGET','http://twitter.com/home?status=CONTENT','2010-09-22 18:57:17','2010-09-22 18:57:17'),(2,'twitter-reply','twitter-reply','twitter',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(3,'twitter-message','twitter-dm','twitter',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(4,'facebook','facebook-share','facebook','http://www.facebook.com/sharer.php?u=AWESM_TARGET',NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(5,'facebook-share','facebook-share','facebook','http://www.facebook.com/sharer.php?u=AWESM_TARGET',NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(6,'facebook-like','facebook-like','facebook',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(7,'facebook-post','facebook-post','facebook',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(8,'facebook-status','facebook-status','facebook',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(9,'facebook-message','facebook-message','facebook',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(10,'myspace','myspace-share','myspace','http://www.myspace.com/index.cfm?fuseaction=postto&u=AWESM_TARGET&c=CONTENT',NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(11,'myspace-share','myspace-share','myspace','http://www.myspace.com/index.cfm?fuseaction=postto&u=AWESM_TARGET&c=CONTENT',NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(12,'myspace-post','myspace-post','myspace',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(13,'linkedin','linkedin-share','linkedin','http://www.linkedin.com/shareArticle?mini=true&url=AWESM_TARGET&summary=CONTENT',NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(14,'linkedin-share','linkedin-share','linkedin','http://www.linkedin.com/shareArticle?mini=true&url=AWESM_TARGET&summary=CONTENT',NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(15,'linkedin-post','linkedin-post','linkedin',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(16,'googlebuzz','googlebuzz','googlebuzz','http://www.google.com/buzz/post?url=AWESM_TARGET&message=CONTENT',NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(17,'email','email','email','mailto:?body=CONTENT%20AWESM_TARGET','mailto:?body=CONTENT','2010-09-22 18:57:17','2010-09-22 18:57:17'),(18,'copypaste','copypaste','copypaste',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(19,'static','static','static',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17'),(20,'test','test','testing',NULL,NULL,'2010-09-22 18:57:17','2010-09-22 18:57:17');
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-04-13 19:49:03
