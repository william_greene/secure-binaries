-- MySQL dump 10.11
--
-- Host: localhost    Database: awesm_production
-- ------------------------------------------------------
-- Server version	5.0.75-0ubuntu10.3-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `redirections`
--

DROP TABLE IF EXISTS `redirections`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `redirections` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `content_id` int(11) unsigned default NULL,
  `parent_awesm` varchar(255) character set latin1 collate latin1_bin default NULL,
  `stub` varchar(255) character set latin1 collate latin1_bin NOT NULL,
  `url` text character set utf8 collate utf8_bin,
  `original_url` text character set utf8 collate utf8_bin,
  `share_type` varchar(255) default 'other',
  `create_type` varchar(255) default 'api',
  `snowball_id` varchar(255) character set utf8 collate utf8_bin default NULL,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  `account_id` int(11) unsigned default NULL,
  `sanitized_url` text character set utf8 collate utf8_bin,
  `domain` varchar(255) default 'awe.sm',
  `user_id` varchar(255) character set utf8 collate utf8_bin default NULL,
  `notes` varchar(255) default NULL,
  `parent_id` bigint(20) unsigned default NULL,
  `original_url_id` bigint(20) unsigned default NULL,
  `domain_id` int(11) unsigned default NULL,
  `campaign_id` int(11) unsigned default NULL,
  `post_id` varchar(255) character set utf8 collate utf8_bin default NULL,
  `conversion_1` varchar(255) character set utf8 default NULL,
  `conversion_2` varchar(255) character set utf8 default NULL,
  `conversion_3` varchar(255) character set utf8 default NULL,
  `conversion_source_id` int(11) default NULL,
  `is_real` tinyint(1) default '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `stub_domain_idx` (`stub`,`domain`),
  KEY `index_redirections_on_parent_id` (`parent_awesm`),
  KEY `index_on_redirections_account_id` (`account_id`),
  KEY `index_on_redirections_snowball_id` (`snowball_id`),
  KEY `index_on_redirections_domain` (`domain`),
  KEY `index_on_redirections_created_at` (`created_at`),
  KEY `index_on_redirections_share_type` (`share_type`),
  KEY `index_on_redirections_create_type` (`create_type`),
  KEY `index_on_redirections_user_id` (`user_id`(20)),
  KEY `index_on_redirections_notes` (`notes`(20)),
  KEY `index_on_redirections_domain_id` (`domain_id`),
  KEY `index_on_redirections_original_url_id` (`original_url_id`),
  KEY `index_on_redirections_parent_id` (`parent_id`),
  KEY `index_on_redirections_post_id` (`post_id`),
  KEY `index_on_redirections_campaign_id` (`campaign_id`),
  KEY `index_on_redirections_sanitized_url` (`sanitized_url`(255)),
  KEY `index_on_redirections_content_id` (`content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60393104 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-04-13 19:49:07
