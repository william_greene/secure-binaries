-- MySQL dump 10.11
--
-- Host: localhost    Database: awesm_production
-- ------------------------------------------------------
-- Server version	5.0.75-0ubuntu10.3-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_subscriptions`
--

DROP TABLE IF EXISTS `account_subscriptions`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `account_subscriptions` (
  `id` int(11) NOT NULL auto_increment,
  `account_id` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `is_primary` bit(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `index_on_account_id` (`account_id`),
  KEY `index_on_subscription_id` (`subscription_id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `account_subscriptions`
--

LOCK TABLES `account_subscriptions` WRITE;
/*!40000 ALTER TABLE `account_subscriptions` DISABLE KEYS */;
INSERT INTO `account_subscriptions` VALUES (4,948,4,''),(8,958,8,''),(10,960,10,''),(11,957,11,''),(12,963,12,''),(13,964,13,''),(14,965,14,''),(20,972,20,''),(21,973,21,''),(25,979,25,''),(27,982,27,''),(28,983,28,''),(29,984,29,''),(33,988,33,''),(41,998,41,''),(43,1000,43,''),(44,1001,44,''),(46,1004,46,''),(54,1016,54,''),(56,1018,56,''),(57,1019,57,''),(58,1022,58,''),(60,1025,60,''),(62,1027,62,''),(68,1036,68,''),(69,1037,69,''),(70,1039,70,''),(73,1041,73,''),(78,1052,78,''),(85,1054,85,''),(88,1058,88,''),(89,1059,89,''),(91,1061,91,''),(92,1063,92,''),(93,1068,93,''),(94,1069,94,''),(96,1073,96,''),(97,1076,97,''),(98,1077,98,''),(99,1078,99,''),(100,1079,100,''),(101,1080,101,''),(102,1082,102,''),(103,1083,103,''),(104,1084,104,''),(105,1085,105,''),(106,1086,106,''),(107,1087,107,''),(108,1089,108,''),(109,1090,109,''),(110,1091,110,''),(111,1094,111,''),(112,1095,112,''),(113,1096,113,''),(114,1097,114,''),(115,1098,115,'');
/*!40000 ALTER TABLE `account_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-04-13 19:49:03
