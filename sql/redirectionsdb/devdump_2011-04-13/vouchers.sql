-- MySQL dump 10.11
--
-- Host: localhost    Database: awesm_production
-- ------------------------------------------------------
-- Server version	5.0.75-0ubuntu10.3-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `vouchers`
--

DROP TABLE IF EXISTS `vouchers`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(45) NOT NULL,
  `code` varchar(200) NOT NULL,
  `display_name` varchar(500) NOT NULL,
  `parameters` varchar(2000) NOT NULL,
  `updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `vouchers`
--

LOCK TABLES `vouchers` WRITE;
/*!40000 ALTER TABLE `vouchers` DISABLE KEYS */;
INSERT INTO `vouchers` VALUES (1,'package-change','awesm4fr33','Extended free trial','{\"package_id\":8}','2010-10-15 01:42:47','2010-10-15 01:42:47'),(3,'package-change','free047401','Extended free trial','{\"package_id\":8}','2011-02-04 22:48:25','2011-02-04 22:48:25'),(4,'package-change','free948247','Extended free trial','{\"package_id\":8}','2011-02-04 22:48:25','2011-02-04 22:48:25'),(5,'package-change','free101071','Extended free trial','{\"package_id\":8}','2011-02-04 22:48:25','2011-02-04 22:48:25'),(6,'package-change','free985723','Extended free trial','{\"package_id\":8}','2011-02-04 22:48:25','2011-02-04 22:48:25'),(7,'package-change','free093821','Extended free trial','{\"package_id\":8}','2011-02-04 22:48:25','2011-02-04 22:48:25'),(8,'package-change','free011175','Extended free trial','{\"package_id\":8}','2011-02-04 22:48:25','2011-02-04 22:48:25'),(9,'package-change','free097453','Extended free trial','{\"package_id\":8}','2011-02-04 22:48:25','2011-02-04 22:48:25'),(10,'package-change','free107754','Extended free trial','{\"package_id\":8}','2011-02-04 22:48:25','2011-02-04 22:48:25'),(11,'package-change','free234891','Extended free trial','{\"package_id\":8}','2011-02-04 22:48:25','2011-02-04 22:48:25'),(12,'package-change','free625093','Extended free trial','{\"package_id\":8}','2011-02-04 22:48:25','2011-02-04 22:48:25');
/*!40000 ALTER TABLE `vouchers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-04-13 19:49:04
